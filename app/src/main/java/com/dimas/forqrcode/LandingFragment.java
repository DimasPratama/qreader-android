package com.dimas.forqrcode;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class LandingFragment extends Fragment implements View.OnClickListener {

    Button triggerScanButton, logoutButton;
    TextView emailTextView;
    FirebaseAuth mAuth;

    public LandingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_landing, container, false);

        mAuth = FirebaseAuth.getInstance();
        triggerScanButton = (Button) view.findViewById(R.id.button_scan);
        triggerScanButton.setOnClickListener(this);
        emailTextView = (TextView) view.findViewById(R.id.textView_landing_email);
        emailTextView.setText(mAuth.getCurrentUser().getEmail());
        logoutButton = (Button) view.findViewById(R.id.button_logout);
        logoutButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_scan){
            Intent naviagteToMainActivity = new Intent(getActivity(), MainActivity.class);
            startActivity(naviagteToMainActivity);
        } else if (v.getId() == R.id.button_logout){
            System.out.println("Tapped button logout");
            handleUserLogout();
        }
    }

    private void handleUserLogout() {
        mAuth.signOut();

        Intent navigateToLoginActivity = new Intent(getActivity(), LoginActivity.class);
        startActivity(navigateToLoginActivity);
        getActivity().finish();
    }
}
