package com.dimas.forqrcode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebContentActivity extends AppCompatActivity {

    WebView contentFromScannedURL;
    String scannedURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_content);

        contentFromScannedURL = (WebView) findViewById(R.id.webView_contentFromScannedURL);
        scannedURL = getIntent().getStringExtra("url");

        contentFromScannedURL.loadUrl(scannedURL);
    }
}
