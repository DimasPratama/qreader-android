package com.dimas.forqrcode;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ContentActivity extends Activity {

    String content = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        content = getIntent().getStringExtra("content");

        TextView ContentForText = (TextView) findViewById(R.id.contentText);
        if(getIntent().getExtras()!=null){
            ContentForText.setText(content);
        }
    }
}
