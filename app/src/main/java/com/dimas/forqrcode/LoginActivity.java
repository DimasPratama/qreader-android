package com.dimas.forqrcode;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button signInButton;
    EditText emailEditText;
    EditText passwordEditText;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        signInButton = (Button) findViewById(R.id.button_login);
        signInButton.setOnClickListener(this);

        emailEditText = (EditText) findViewById(R.id.editText_login_email);
        passwordEditText = (EditText) findViewById(R.id.editText_login_password);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() != null){
            Intent navigateToLandingPage = new Intent(LoginActivity.this, TabBarActivity.class);
            LoginActivity.this.startActivity(navigateToLandingPage);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_login){
            String emailInput = emailEditText.getText().toString();
            String passwordInput = passwordEditText.getText().toString();

            if (emailInput.length() == 0 && passwordInput.length() == 0){
                emailEditText.setError("This field cannot be empty");
                passwordEditText.setError("This field cannot be empty");
            }
            else if (emailInput.length() == 0){
                emailEditText.setError("This field cannot be empty");
            } else if (passwordInput.length() == 0){
                passwordEditText.setError("This field cannot be empty");
            } else {
                handleUserLogin(emailInput, passwordInput);
            }
        }
    }

    private void handleUserLogin(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    System.out.println("Logged in with email: "+mAuth.getCurrentUser().getEmail());
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LoginActivity.this);
                    alertBuilder.setTitle("Signed in")
                            .setMessage("Welcome, "+mAuth.getCurrentUser().getEmail()+"!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent navigateToLandingPage = new Intent(LoginActivity.this, TabBarActivity.class);
                                    LoginActivity.this.startActivity(navigateToLandingPage);
                                    finish();
                                }
                            })
                            .show();
                } else {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LoginActivity.this);
                    alertBuilder.setTitle("Failed to log in")
                            .setMessage("Please enter a correct credential!")
                            .setPositiveButton("Okay", null)
                            .show();
                }
            }
        });
    }
}
